<footer class="sports__footer">
	<div class="section__constrained">

		<div class="pure-g">
			<div class="pure-u-1 pure-u-md-1-3" data-aos="fade" data-aos-delay="0" data-aos-duration="500">


				<em>Part of Levy UK</em>

				<div class="sports__social">
					<ul class="wrapper__flex">
						<li class="social-item">
							<a href="/">
								<i class="icon-facebook"></i>
							</a>
						</li>
						<li class="social-item">
							<a href="/">
								<i class="icon-twitter"></i>
							</a>
						</li>
						<li class="social-item">
							<a href="/">
								<i class="icon-linkedin"></i>
							</a>
						</li>
					</ul>
				</div>

				<ul class="terms__wrapper">
					<li><a href="#">Terms & Condirions</a></li>
					<li><a href="#">Privacy Policy</a></li>
				</ul>

			</div>
			<div class="pure-u-1 pure-u-md-1-3" data-aos="fade" data-aos-delay="100" data-aos-duration="500">
				<div class="sports__footer--nav">
					<em>Menu</em>
					<ul>
						<li><a href="#">About Us</a></li>
						<li><a href="#">Our Facilities</a></li>
						<li><a href="#">Event Types</a></li>
						<li><a href="#">Latest</a></li>
						<li><a href="#">Contact</a></li>
					</ul>
				</div>
			</div>
			<div class="pure-u-1 pure-u-md-1-3" data-aos="fade" data-aos-delay="200" data-aos-duration="500">
				<div class="sports__footer--nav">
					<em>Contact</em>
					<div class="address--wrapper">
						<span>Address line 1</span>
						<span>Address line 2</span>
						<span>Address line 3</span>

						<ul>
							<li>
								<a href="mailto:example@example.co.uk">example@example.co.uk</a>
							</li>
							<li>
								<a href="tel:01234 56789">01234 56789</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
