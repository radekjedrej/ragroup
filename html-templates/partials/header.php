<header class="l-header">

	<div id="sports__topbar">
		<div class="section__constrained" data-aos="fade" data-aos-delay="0" data-aos-duration="500">
			<div>
				<a href="mailto:example@example.co.uk">example@example.co.uk</a>
			</div>
			<div>
				<a href="tel:01234 56789">01234 56789</a>
			</div>
<!--			<div class="header--social">-->
<!--				<ul class="wrapper__flex">-->
<!--					<li class="social-item">-->
<!--						<a href="/">-->
<!--							<i class="icon-faebook"></i>-->
<!--						</a>-->
<!--					</li>-->
<!--					<li class="social-item">-->
<!--						<a href="/">-->
<!--							<i class="icon-twitter"></i>-->
<!--						</a>-->
<!--					</li>-->
<!--				</ul>-->
<!--			</div>-->
		</div>
	</div>

	<div id="sports__header--wrapper" class="section__constrained">
		<div class="wrapper__flex space center--align">



			<nav id="sports__nav" data-aos="fade" data-aos-delay="200" data-aos-duration="500">
				<div class="ham">
					<div></div>
					<div></div>
					<div></div>
				</div>
				<div class="main__navigation">
					<ul>
						<li><a href="#">About Us</a></li>
						<li><a href="#">Our Facilities</a></li>
						<li><a href="#">Event Types</a></li>
						<li><a href="#">Latest</a></li>
						<li><a href="#">Contact</a></li>
					</ul>
				</div>
			</nav>
		</div>
	</div>
</header>